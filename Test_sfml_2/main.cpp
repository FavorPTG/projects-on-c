#include <SFML/Graphics.hpp>

int main()
{
    sf::RenderWindow app(sf::VideoMode(800, 600), "SFML window");

    sf::Texture texture;
    if (!texture.loadFromFile("cb.bmp"))
        return EXIT_FAILURE;
    sf::Sprite sprite(texture);

    while (app.isOpen())
    {
        sf::Event event;
        while (app.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                app.close();
        }

        app.clear();

        app.draw(sprite);

        app.display();
    }

    return EXIT_SUCCESS;
}
