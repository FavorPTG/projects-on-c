#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

struct SCamera{
    float x,y,z;
    float Xrot, Zrot;
} camera;

void Applycamera();
void rotateCamera(float xAngle, float zAngle);
void Camera_AutoMoveByMouse(int centerX, int centerY, float speed);
void Camera_MoveDirecrion(int forwardMove, int rightMove, float speed);

#endif // CAMERA_H_INCLUDED
