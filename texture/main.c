#include <windows.h>
#include <gl/gl.h>
#include <math.h>

#define STB_IMAGE_IMPLEMENTATION
#include "../_STB_Image/stb_image.h"

#define Wwidth 182
#define Wheight 256

LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
void EnableOpenGL(HWND hwnd, HDC*, HGLRC*);
void DisableOpenGL(HWND, HDC, HGLRC);

unsigned int texture;//, tex2;

static float rotX = -60;
static float rotZ = 33;
float color = 1.0f;
BOOL left = TRUE;

void RotWorld(){
    if(GetKeyState(VK_UP) < 0) rotX++;
    if(GetKeyState(VK_DOWN) < 0) rotX--;
    if(GetKeyState(VK_LEFT) < 0) rotZ--;
    if(GetKeyState(VK_RIGHT) < 0) rotZ++;
}

float ret(){
    if(left == TRUE) color -= 0.01;
    if(color <= 0) left = FALSE;
    if(left == FALSE) color += 0.01;
    if(color >= 1) left = TRUE;
}

void Load_Texture(char *fileName, int *textureID){

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    int width, height, cnt;
    unsigned char *data = stbi_load(fileName, &width, &height, &cnt, 0);

    glGenTextures(1, textureID);
    glBindTexture(GL_TEXTURE_2D, *textureID);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, cnt == 4 ? GL_RGBA : GL_RGB, width, height,
                                    0, cnt == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data);
    glBindTexture(GL_TEXTURE_2D, 0);
    stbi_image_free(data);
}

void Game_Init(){
    Load_Texture("Poster.jpg", &texture);
    //Load_Texture("Poster.jpg", &tex2);

    glLoadIdentity();
    glFrustum(-0.1,0.1, -0.1,0.1, 0.2,1000);

    glEnable(GL_DEPTH_TEST);
}

float vertex[] = {-1,-1,0, 1,-1,0, 1,1,0, -1,1,0,
                  -1,-1,2, 1,-1,2, 1,1,2, -1,1,2};
float texCoord[] = {0,1, 1,1, 0,1, 1,1,
                    0,0, 1,0, 0,0, 1,0};
GLuint texInd[] = {0,1,5, 5,4,0, 1,2,6, 6,5,1, 2,3,7, 7,6,2, 3,0,4, 4,7,3};
int texIndCnt = sizeof(texInd) / sizeof(GLuint);

float texCoord2[] = {0,1, 1,1, 1,0, 0,0,
                     0,1, 1,1, 1,0, 0,0};
GLuint texInd2[] = {0,1,2, 2,3,0, 4,5,6, 6,7,4};
int texIndCnt2 = sizeof(texInd2) / sizeof(GLuint);

    float pyramid[] = {0,0,1, -1,-1,0, 1,-1,0, 1,1,0, -1,1,0};
    float pyramidUV[] = {0.5,0, 0,1, 1,1, 0,1, 1,1};
    GLuint pyramidInd[] = {0,1,2, 0,2,3, 0,3,4, 0,4,1};
    int pyramidIndCnt = sizeof(pyramidInd) / sizeof(GLuint);

void Game_Show(float color){

    ret();

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);

    glColor4f(1,1,1, color);

    glPushMatrix();
        //
        glRotatef(rotX, 1,0,0);
        glRotatef(rotZ, 0,0,1);
        glPushMatrix();
            RotWorld();
        glPopMatrix();
        glTranslatef(2,3,-2.8);//2);

        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);

            glVertexPointer(3, GL_FLOAT, 0, vertex);
            glTexCoordPointer(2, GL_FLOAT, 0, texCoord);
            glPushMatrix();
                glScalef(1,1,1); //0.6);
                glDrawElements(GL_TRIANGLES, texIndCnt, GL_UNSIGNED_INT, texInd);
                glTexCoordPointer(2, GL_FLOAT, 0, texCoord2);
                glDrawElements(GL_TRIANGLES, texIndCnt2, GL_UNSIGNED_INT, texInd2);
            glPopMatrix();

            glVertexPointer(3, GL_FLOAT, 0, pyramid);
            glTexCoordPointer(2, GL_FLOAT, 0, pyramidUV);
            glPushMatrix();
                glTranslatef(0,0, 2);
                glDrawElements(GL_TRIANGLES, pyramidIndCnt, GL_UNSIGNED_INT, pyramidInd);
            glPopMatrix();

        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glPopMatrix();
}

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nCmdShow)
{
    WNDCLASSEX wcex;
    HWND hwnd;
    HDC hDC;
    HGLRC hRC;
    MSG msg;
    BOOL bQuit = FALSE;
    float theta = 0.0f;

    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_OWNDC;
    wcex.lpfnWndProc = WindowProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = "GLSample";
    wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);;


    if (!RegisterClassEx(&wcex))
        return 0;

    hwnd = CreateWindowEx(0,
                          "GLSample",
                          "OpenGL Sample",
                          WS_OVERLAPPEDWINDOW,
                          CW_USEDEFAULT,
                          CW_USEDEFAULT,
                          Wwidth*2,
                          Wheight*2,
                          NULL,
                          NULL,
                          hInstance,
                          NULL);

    ShowWindow(hwnd, nCmdShow);

    EnableOpenGL(hwnd, &hDC, &hRC);

    Game_Init();

    while (!bQuit)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
            {
                bQuit = TRUE;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            //
            Game_Show(color);
            //

            SwapBuffers(hDC);

            theta += 1.0f;
            Sleep (1);
        }
    }

    DisableOpenGL(hwnd, hDC, hRC);

    DestroyWindow(hwnd);

    return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
        case WM_CLOSE:
            PostQuitMessage(0);
        break;

        case WM_DESTROY:
            return 0;

        case WM_KEYDOWN:
        {
            switch (wParam)
            {
                case VK_ESCAPE:
                    PostQuitMessage(0);
                break;
            }
        }
        break;

        default:
            return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }

    return 0;
}

void EnableOpenGL(HWND hwnd, HDC* hDC, HGLRC* hRC)
{
    PIXELFORMATDESCRIPTOR pfd;

    int iFormat;

    *hDC = GetDC(hwnd);

    ZeroMemory(&pfd, sizeof(pfd));

    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW |
                  PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;

    iFormat = ChoosePixelFormat(*hDC, &pfd);

    SetPixelFormat(*hDC, iFormat, &pfd);

    *hRC = wglCreateContext(*hDC);

    wglMakeCurrent(*hDC, *hRC);
}

void DisableOpenGL (HWND hwnd, HDC hDC, HGLRC hRC)
{
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(hRC);
    ReleaseDC(hwnd, hDC);
}

