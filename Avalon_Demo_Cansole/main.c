#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

#define width 120 //������
#define height 30 //������

struct {
    char map[height][width+1];
}loc;

void Load_location(char *fileName){
    memset(&loc.map, ' ', sizeof(loc));
    for (int i = 0; i < height; i++)
        loc.map[i][width] = '\0';

    FILE *f = fopen(fileName, "r");
    char c[20];
    int line = 0;
    while(!feof(f)){
        fgets(c, width, f);
        int cnt = strlen(c);
        if (c[cnt-1] == '\n') cnt--;
        strncpy(loc.map[line], c, cnt);
        line++;
    }
    fclose(f);

    loc.map[height-1][width-1] = '\0';
}

void SetCurPos(){
    COORD coord;
    coord.X = 0;
    coord.Y = 0;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

char map[height][width+1];

void map_show(){
    SetCurPos();
    for (int i = 0; i < height; i++)
        printf("%s", map[i]);
}

void loc_PutOnMap(){
    memcpy(map, loc.map, sizeof(map));
}

struct {
    char name[20];
    POINT pos;
}playre;

void Playre_init(int x, int y, char *name){
    playre.pos.x = x;
    playre.pos.y = y;
    sprintf(playre.name, name);
}

void playre_PutOnMap(){
    map[playre.pos.y][playre.pos.x] = '@';
}

void playre_move(){
    POINT old = playre.pos;
    if(GetKeyState('W') < 0) playre.pos.y--;
    if(GetKeyState('S') < 0) playre.pos.y++;
    if(GetKeyState('A') < 0) playre.pos.x--;
    if(GetKeyState('D') < 0) playre.pos.x++;
    if(map[playre.pos.y][playre.pos.x] != ' ')
        playre.pos = old;
}

void playre_Save(){
    FILE *f = fopen(playre.name, "wb");
        fwrite(&playre, 1, sizeof(playre), f);
    fclose(f);
}

void playre_Load(char *name){
    FILE *f = fopen(name, "rb");
    if(f == NULL)
        Playre_init(17, 10, name);
    else
        fread(&playre, 1, sizeof(playre), f);
    fclose(f);
}

int main()
{
    playre_Load("Favor");
    Load_location("Config.txt");
    do{
        playre_move();
        loc_PutOnMap();
        playre_PutOnMap();
        map_show();
        Sleep(60);
    }
    while(GetKeyState(VK_ESCAPE) >= 0);

    playre_Save();

    return 0;
}
