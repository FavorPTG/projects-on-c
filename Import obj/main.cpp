#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void ReadFile(char* name){
    fstream FileObj;

    FileObj.open(name, ios::in);

    if(FileObj.is_open()){
        string readLine;
        while(!FileObj.eof()){
            getline(FileObj, readLine, '\n');
            char V1 = readLine.c_str()[0],
                V2 = readLine.c_str()[1];

            switch(V1){
                case 'v':
                    switch(V2){
                    case ' ':
                        cout<<"\n Point \t";
                        break;
                    case 'n':
                        cout<<"\n Normal \t";
                        break;
                    case 't':
                        cout<<"\n Texture \t";
                        break;
                    default:
                        break;
                    }
                break;
                default:
                    break;
            }
        }
    }
}

int main()
{
    cout<<"Read Obj Format"<<endl;
    ReadFile("test.obj");
    return 0;
}
